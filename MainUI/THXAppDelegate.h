//
//  THXAppDelegate.h
//  MainUI
//
//  Created by Admin mac on 9/8/14.
//  Copyright (c) 2014 iba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface THXAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
