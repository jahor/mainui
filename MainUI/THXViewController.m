//
//  THXViewController.m
//  MainUI
//
//  Created by Admin mac on 9/8/14.
//  Copyright (c) 2014 iba. All rights reserved.
//

#import "THXViewController.h"

@interface THXViewController ()
@property (weak, nonatomic) IBOutlet UITextField *userid;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIView *loginContainer;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@end

@implementation THXViewController

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [self.userid resignFirstResponder];
    [self.password resignFirstResponder];
    return YES;
}

- (IBAction)touchLogIn {
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"fromLogin"]) {
        if ([self.userid.text isEqualToString:@"admin"]
            && [self.password.text isEqualToString:@"admin"]) {
            self.errorLabel.text = @"";
            return YES;
        }
    }
    self.errorLabel.text = @"User ID or password is incorrect!";
    return NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.userid setDelegate:self];
    [self.password setDelegate:self];
	self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login-bg.png"]];
    self.loginContainer.layer.masksToBounds = NO;
    self.loginContainer.layer.borderColor = [UIColor grayColor].CGColor;
    self.loginContainer.layer.borderWidth = 0.5;
//    self.loginContainer.layer.cornerRadius = 10;
    self.loginContainer.layer.shadowOffset = CGSizeMake(0, 0);
    self.loginContainer.layer.shadowRadius = 10;
    self.loginContainer.layer.shadowOpacity = 0.35;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
