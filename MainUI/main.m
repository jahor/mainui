//
//  main.m
//  MainUI
//
//  Created by Admin mac on 9/8/14.
//  Copyright (c) 2014 iba. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "THXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([THXAppDelegate class]));
    }
}
