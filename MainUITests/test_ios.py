import os

import unittest
from appium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

APP_PATH = os.path.join(os.path.realpath(__file__), '../../build/Release-iphonesimulator/MainUI.app')


class Utils():
	@staticmethod
	def set_text_field_value(driver, text_field, text):
		text_field.set_value(text)
		driver.hide_keyboard(key_name = 'Done')


class PageBase(object):
	def __init__(self, driver):
		self.driver = driver

	def wait_for_veiw(self):
		pass


class LogInPage(PageBase):
	def wait_for_view(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'Log In')))

	def login_field(self):
		return self.driver.find_elements_by_class_name('UIATextField')[0]

	def psw_field(self):
		return self.driver.find_elements_by_class_name('UIATextField')[1]

	def input_login(self, login):
		Utils.set_text_field_value(self.driver, self.login_field(), login)

	def input_password(self, password):
		Utils.set_text_field_value(self.driver, self.psw_field(), password)

	def press_log_in(self):
		submit_btn = self.driver.find_element_by_name('Log In')
		submit_btn.click()

	def error_message_text(self):
		return self.driver.find_element_by_ios_uiautomation('.elements()[1]').text


class MainMenu(PageBase):
	def wait_for_view(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'Touchpoints')))

	def navigate_to_page(self, page):
		menu_item = self.driver.find_element_by_name(page)
		menu_item.click()

	def return_to_main_menu(self):
		self.driver.find_element_by_name('One Connect').click()


class TouchpointsPage(PageBase):
	def wait_for_view(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'ADD TOUCHPOINT')))

	def wait_for_touchpoints_tab(self):
		self.wait_for_view()

	def wait_for_interactions_tab(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'ADD INTERACTION')))

	def wait_for_testtouchpoints_tab(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, 'EXECUTE')))

	def switch_to_tab(self, tab):
		self.driver.find_element_by_name(tab).click()


class ActionsAndAssetsPage(PageBase):
	def wait_for_view(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, '+ CREATE AN ACTION')))

	def wait_for_actions_tab(self):
		self.wait_for_view()

	def wait_for_assets_tab(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.NAME, '+ CREATE AN ASSET')))

	def switch_to_tab(self, tab):
		self.driver.find_element_by_name(tab).click()


class LoginTest(unittest.TestCase):
	def setUp(self):
		desired_caps = {}
		desired_caps['platformName'] = 'iOS'
		desired_caps['platformVersion'] = '7.1'
		desired_caps['deviceName'] = 'iPhone Simulator'
		desired_caps['app'] = APP_PATH

		self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

		self.loginPage = LogInPage(self.driver)
		self.mainMenu = MainMenu(self.driver)
		self.touchpoints = TouchpointsPage(self.driver)
		self.actionsAndAssets = ActionsAndAssetsPage(self.driver)

	def tearDown(self):
		self.driver.quit()

	def test_the_app(self):
		self.checkLogin()

		self.mainMenu.wait_for_view()
		self.mainMenu.navigate_to_page("Touchpoints")
		
		self.checkTouchpoints()

		self.mainMenu.return_to_main_menu()
		self.mainMenu.wait_for_veiw()
		self.mainMenu.navigate_to_page("Actions & Assets")

		self.checkActionsAndAssets()

	def checkLogin(self):
		self.loginPage.wait_for_view()
		self.loginPage.input_login("invalid")
		self.loginPage.input_password("admin")
		self.loginPage.press_log_in()
		self.assertEquals(self.loginPage.error_message_text(), 'User ID or password is incorrect!', 'User credentials should be illegal')
		# input correct user id
		self.loginPage.input_login("admin")
		self.loginPage.press_log_in()

	def checkTouchpoints(self):
		self.touchpoints.wait_for_view()
		self.touchpoints.switch_to_tab('Interactions')
		self.touchpoints.wait_for_interactions_tab()
		self.touchpoints.switch_to_tab('Test a Touchpoint')
		self.touchpoints.wait_for_testtouchpoints_tab()

	def checkActionsAndAssets(self):
		self.actionsAndAssets.wait_for_veiw()
		self.actionsAndAssets.switch_to_tab('Assets')
		self.actionsAndAssets.wait_for_assets_tab()
		self.actionsAndAssets.switch_to_tab('Actions')
		self.actionsAndAssets.wait_for_actions_tab()


if __name__ == "__main__":
	# building the app for ios simulator
	from subprocess import Popen, PIPE
	project_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../')
	building_process = Popen('xcodebuild -arch i386 -sdk iphonesimulator7.1', stdout=PIPE, universal_newlines=True, cwd=project_path, shell=True)
	print(building_process.communicate()[0])

	suite = unittest.TestLoader().loadTestsFromTestCase(LoginTest)
	unittest.TextTestRunner(verbosity=2).run(suite)